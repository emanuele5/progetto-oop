import javax.imageio.ImageIO;
import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import javax.sound.sampled.Clip;

import static java.awt.event.KeyEvent.*;

public class Game extends JPanel implements ActionListener, KeyListener {
	JFrame parent;
	Timer timer;
	Random random=new Random();
	boolean paused=false;
	// game objects
	Status status=new Status();
	Player player=new Player();
	ArrayList<PlayerLaser> playerLasers;
	ArrayList<Alien> aliens;
	ArrayList<AlienLaser> alienLasers;
	ArrayList<GameObject> animations=new ArrayList<>();
	// key status (only way to avoid key repetition, as it's an os functionality)
	boolean spacePressed=false;
	boolean leftPressed=false;
	boolean rightPressed=false;

	static Clip clip;

	static {
		try {
			AudioInputStream inputStream = AudioSystem.getAudioInputStream(new File("src/sounds/level.wav"));
			clip = AudioSystem.getClip();
			clip.open(inputStream);
		} catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
			e.printStackTrace();
		}
	}

	Game(JFrame parent)  {
		super();
		this.parent=parent;
		timer = new Timer(20, this);

		generateLevel();

		setBackground(Color.BLACK);
		addKeyListener(this);
		setFocusable(true);
	}

	void generateLevel() {
		status.addLevel();
		if(status.getLevel()>1){
			animations.add(new LevelUp());
		}
		playerLasers = new ArrayList<>();
		aliens = new ArrayList<>();
		alienLasers = new ArrayList<>();
		if(status.getLevel()!=1 && status.getLevel()%3==0){
			if(status.getLives()<3){
				status.addLife();
				player.setSprite(status.getLives());
				Sound.RunShots("src/sounds/recupero.wav");
			}
			else if(status.getShields()<5){
				status.addShield();
				Sound.RunShots("src/sounds/recupero.wav");
			}
		}
		for (int i = 0; i < 10*status.getLevel(); i++) {
			var alien = new Alien(status.getLevel());
			aliens.add(alien);
		}
	}

	public void start() {
		timer.start();
		clip.loop(Clip.LOOP_CONTINUOUSLY);
	}

	void stop() {
		clip.stop();
		timer.stop();
	}

	@Override
	public void actionPerformed(ActionEvent actionEvent) {
		if (actionEvent.getSource() == timer) {
			// update objects
			for (GameObject o : animations) {
				o.Update();
			}
			for (PlayerLaser l : playerLasers) {
				l.Update();
			}
			for (AlienLaser l : alienLasers) {
				l.Update();
			}
			for (Alien o : aliens) {
				if((o.getX()>=(640-o.larghezza) && o.speedX>0) || (o.getX()<=0 && o.speedX<0)) {
					o.speedX*=-1;
				}
				o.Update();
			}
			player.Update();
			// check for collisions
			CheckCollisions();
			CheckCollisionsPlayer();
			Check();
			// remove deleted objects
			playerLasers.removeIf(GameObject::isDeleted);
			alienLasers.removeIf(GameObject::isDeleted);
			aliens.removeIf(GameObject::isDeleted);
			animations.removeIf(GameObject::isDeleted);
			// make aliens shoot
			for (Alien o: aliens) {
				if(random.nextInt(500-(status.getLevel()))==1) {
					AlienLaser laser= (AlienLaser) o.Shoot();
					alienLasers.add(laser);
				}
			}
			// generate new level if no aliens left
			if(aliens.size()==0) {
				generateLevel();
			}
			status.Update();
			repaint();
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		BackgroundImage.Draw(g);
		for (PlayerLaser o : playerLasers) {
			o.Draw((Graphics2D) g);
		}
		for (AlienLaser o : alienLasers) {
			o.Draw((Graphics2D) g);
		}
		for (Alien o : aliens) {
			o.Draw((Graphics2D) g);
		}
		for(GameObject o : animations){
			o.Draw((Graphics2D) g);
		}
		player.Draw((Graphics2D) g);
		status.Draw((Graphics2D) g);
	}

	@Override
	public void keyTyped(KeyEvent keyEvent) {
		// empty
	}

	@Override
	public void keyPressed(KeyEvent keyEvent) {
		if (keyEvent.getKeyCode() == VK_LEFT && !leftPressed) {
			player.setSpeedX(player.getSpeedX() - 4);
			leftPressed = true;
		}
		if (keyEvent.getKeyCode() == VK_RIGHT && !rightPressed) {
			player.setSpeedX(player.getSpeedX() + 4);
			rightPressed = true;
		}
		if (keyEvent.getKeyCode() == VK_SPACE && !spacePressed) {
			PlayerLaser laser = (PlayerLaser) player.Shoot();
			playerLasers.add(laser);
			spacePressed = true;
		}
		if (keyEvent.getKeyCode() == VK_P) {
			if(paused) {
				start();
				paused=false;
			}
			else {
				stop();
				paused=true;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent keyEvent) {
		if (keyEvent.getKeyCode() == VK_LEFT) {
			player.setSpeedX(player.getSpeedX() + 4);
			leftPressed = false;
		}
		if (keyEvent.getKeyCode() == VK_RIGHT) {
			player.setSpeedX(player.getSpeedX() - 4);
			rightPressed = false;
		}
		if (keyEvent.getKeyCode() == VK_SPACE) {
			spacePressed = false;
		}
	}

	public void CheckCollisions () {
		for (PlayerLaser laser : playerLasers) {
			//control collision
			for (Alien alien : aliens) {
				if (laser.collides(alien)) {
					Explosion e= alien.Explode();
					animations.add(e);
					laser.setDeleted(true);
					status.addPoints(10);
					break;
				}
			}
		}
	}

	public void CheckCollisionsPlayer(){
		for(AlienLaser laser : alienLasers){
			if(laser.collides(player)) {
				laser.setDeleted(true);
				if(status.getShields()>0){
					status.removeShield();
					Sound.RunShots("src/sounds/losels.wav");
				}
				else{
					status.removeLife();
					player.setSprite(status.getLives());
					Sound.RunShots("src/sounds/losels.wav");
					if(status.getLives()<=0) {
						DoGameOver();
					}
				}
			}
		}
	}

	public void Check() {
		for (Alien alien : aliens) {
			if (alien.getY() >= 480-alien.altezza) {
				DoGameOver();
			}
		}
	}

	void DoGameOver() {
		stop();
		removeKeyListener(this);
		setFocusable(false);

		// generate game over screen
		GameOverScreen screen=new GameOverScreen(parent,status.getPoints());
		parent.setContentPane(screen);
		parent.revalidate();
		screen.requestFocusInWindow();
	}
}
