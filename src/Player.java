import javax.imageio.ImageIO;
import javax.sound.sampled.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Player extends GameObject implements Shooter {
    // sprite
    private static final BufferedImage[] sprites=new BufferedImage[3];
    private static Clip clip;

    static {
        try {
            AudioInputStream shootSound = AudioSystem.getAudioInputStream(new File("src/sounds/shoot.wav"));
            clip= AudioSystem.getClip();
            clip.open(shootSound);
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
             e.printStackTrace();
        }
    }

    static {
        try {
            sprites[2] = ImageIO.read(new File("src/img/player.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static {
        try {
            sprites[1]= ImageIO.read(new File("src/img/player1.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static {
        try {
            sprites[0]= ImageIO.read(new File("src/img/player2.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private BufferedImage currentSprite=sprites[2];

    public Player() {
        larghezza= 40;
        altezza=40;
        X=350;
        Y=400;
    }

    public void setSprite(int n) {
        if (n >= 1 && n <= 3) {
            currentSprite=sprites[n-1];
        }
    }

    @Override
    public void Update() {
        X+=speedX;
        if(X<0) X=0;
        if(X>640-this.larghezza) X=640-this.larghezza;
    }

    @Override
    public void Draw(Graphics2D g) {
        g.drawImage(currentSprite, null, X, Y);
    }

    public Laser Shoot() {
        clip.setFramePosition(0);
        clip.loop(0);
        return new PlayerLaser(X+10);
    }
}
