import java.awt.*;

public interface Drawable {
    void Draw(Graphics2D g);
}
