import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;
import java.util.ArrayList;

public class GameOverScreen extends JPanel implements KeyListener, MouseListener {
    private final JFrame parent;
    private final int lastPoints;
    private int lastRanking=-1;
    private final ArrayList<Integer> ranking=new ArrayList<>();

    public GameOverScreen(JFrame parent,int points) {
        super();
        this.parent = parent;
        this.lastPoints = points;

        // read top 10 rankings
        try {
            BufferedReader rankingFile = new BufferedReader(new FileReader("src/punteggi.txt"));
            for (int i = 0; i < 10; i++) {
                String line = rankingFile.readLine();
                if (line == null) {
                    break;
                }
                ranking.add(Integer.parseInt(line));
            }
            rankingFile.close();
        } catch (IOException | NumberFormatException e) {
            e.printStackTrace();
        }

        // add new result
        if (ranking.size() == 0) {
            lastRanking=0;
            ranking.add(lastPoints);
        } else if (ranking.size() < 10 && lastPoints < ranking.get(ranking.size() - 1)) {
            lastRanking=ranking.size();
            ranking.add(lastPoints);
        } else {
            for (int i = 0; i < ranking.size(); i++) {
                if (lastPoints >= ranking.get(i)) {
                    lastRanking = i;
                    ranking.add(i, lastPoints);
                    break;
                }
            }
            if(ranking.size()>10) {
                ranking.remove(10);
            }
        }

        // write back new rankings
        try {
            BufferedWriter rankingFile = new BufferedWriter(new FileWriter("src/punteggi.txt"));
            for (Integer integer : ranking) {
                String line = String.valueOf(integer);
                rankingFile.write(line);
                rankingFile.newLine();
            }
            rankingFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        setBackground(Color.BLACK);
        addKeyListener(this);
        addMouseListener(this);
        setFocusable(true);
        repaint();
    }

    void backToMenu() {
        removeKeyListener(this);
        removeMouseListener(this);
        setFocusable(false);

        StartScreen screen=new StartScreen(parent);
        parent.setContentPane(screen);
        parent.revalidate();
        screen.requestFocusInWindow();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        BackgroundImage.Draw2(g);
        g.setColor(Color.RED);
        g.setFont(new Font("DEBUG FREE TRIAL", Font.PLAIN, 60));
        g.drawString("GAME OVER",150,50);
        g.setColor(Color.WHITE);
        g.setFont(new Font("DEBUG FREE TRIAL", Font.BOLD,40));
        g.drawString("Punteggio: "+lastPoints,190,90);
        g.setColor(Color.cyan);
        g.setFont(new Font("DEBUG FREE TRIAL", Font.ITALIC, 25));
        g.drawString("CLASSIFICA: ",20,140);
        // display ranking
        g.setColor(Color.WHITE);
        for(int i=0;i<ranking.size();i++) {
            if(i==lastRanking) {
                g.setColor(Color.cyan);
            }
            g.drawString((i+1)+": "+ranking.get(i),20,180+i*20);
            if(i==lastRanking) {
                g.setColor(Color.WHITE);
            }
        }
        g.setColor(Color.WHITE);
        g.setFont(new Font("DEBUG FREE TRIAL",Font.ITALIC,20));
        g.drawString("Le scimmie spaziali hanno conquistato la terra",190,250);
        g.drawString("ora gli umani possono mangiare solo banane.",190,275);
        g.drawString("La tua reputazione di pilota è distrutta!",190,300);
        g.setColor(Color.RED);
        g.drawRect(20,370,600,80);
        g.drawString("Torna al menù principale",180,420);
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if(keyEvent.getKeyCode()==KeyEvent.VK_ENTER) {
            backToMenu();
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        if(mouseEvent.getX()<=620 && mouseEvent.getX()>=20) {
            if(mouseEvent.getY()<=450 && mouseEvent.getY()>=370) {
                backToMenu();
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
