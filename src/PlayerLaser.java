import java.awt.*;

public class PlayerLaser extends Laser {

	public PlayerLaser(int x) {
		super(x);
		Y=430;
		speedY=-8;
	}

	@Override
	public void Draw(Graphics2D g) {
		g.setColor(Color.RED);
		g.drawLine(X,Y,X+larghezza,Y+altezza);
	}
}
