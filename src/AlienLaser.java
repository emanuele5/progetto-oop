import java.awt.*;

public class AlienLaser extends Laser {

    public AlienLaser(int x, int y) {
        super(x);
        Y=y;
        speedY=4;
    }

    @Override
    public void Draw(Graphics2D g) {
        g.setColor(Color.GREEN);
        g.drawLine(X,Y,X+larghezza,Y+altezza);
    }
}
