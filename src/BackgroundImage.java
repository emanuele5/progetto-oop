import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class BackgroundImage {
    private static BufferedImage image;
    static BufferedImage background;
    static BufferedImage lose;

    static {
        try {
            image = ImageIO.read(new File("src/img/galassia.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static {
        try {
            background = ImageIO.read(new File("src/img/spacebackground.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static {
        try {
            lose = ImageIO.read(new File("src/img/scimmievincenti.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void Draw(Graphics g) {
        g.drawImage(image, 0, 0, null);
    }

    public static void Draw1(Graphics g) {
        g.drawImage(background, 0, 0, null);
    }

    public static void Draw2(Graphics g) {
        g.drawImage(lose, 0, 0, null);
    }

}
