import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class RankingScreen extends JPanel implements MouseListener {

    private final JFrame parent;
    private final ArrayList<String> ranking=new ArrayList<>();

    public RankingScreen(JFrame parent) {
        super();
        this.parent=parent;
        setBackground(Color.BLACK);
        addMouseListener(this);
        setFocusable(true);

        // read top rankings
        try {
            BufferedReader rankingFile = new BufferedReader(new FileReader("src/punteggi.txt"));
            for (int i = 0; i < 10; i++) {
                String line = rankingFile.readLine();
                if (line == null) {
                    break;
                }
                ranking.add(line);
            }
            rankingFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        repaint();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        BackgroundImage.Draw1(g);
        g.setColor(Color.WHITE);
        g.setFont(new Font("DEBUG FREE TRIAL", Font.PLAIN, 30));
        g.drawString("Top 10 migliori punteggi",20,40);
        g.setFont(new Font("DEBUG FREE TRIAL", Font.PLAIN, 15));
        for(int i=0;i<ranking.size();i++) {
            g.drawString((i+1)+": "+ranking.get(i),20,80+i*20);
        }
        g.drawRect(155,300,330,35);
        g.setFont(new Font("DEBUG FREE TRIAL", Font.ITALIC, 25));
        g.drawString("Torna al menù principale",160,330);
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        if(mouseEvent.getY()>=300 && mouseEvent.getY()<=335 && mouseEvent.getX()>=155 && mouseEvent.getX()<=486) {
            removeMouseListener(this);
            setFocusable(false);

            StartScreen screen=new StartScreen(parent);
            parent.setContentPane(screen);
            parent.revalidate();
            screen.requestFocusInWindow();
        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
