import javax.imageio.ImageIO;
import javax.sound.sampled.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Explosion extends GameObject{

    static BufferedImage explosion;
    static Clip clip;
    int count=5;

    static {
        try {
            explosion = ImageIO.read(new File("src/img/explosion.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static {
        try {
            AudioInputStream shootSound = AudioSystem.getAudioInputStream(new File("src/sounds/explosion1.wav"));
            clip= AudioSystem.getClip();
            clip.open(shootSound);
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    public Explosion(int x,int y) {
        X=x;
        Y=y;
        altezza=explosion.getHeight();
        larghezza=explosion.getWidth();
        clip.setFramePosition(0);
        clip.loop(0);
    }

    @Override
    public void Update() {
        count--;
        if (count==0) {
            deleted = true;
        }
    }

    @Override
    public void Draw(Graphics2D g) {
        g.drawImage(explosion,null,X,Y);
    }
}
