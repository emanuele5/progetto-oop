import java.awt.*;

public abstract class GameObject implements Drawable{
    int larghezza;
    int altezza;
    int speedX;
    int speedY;
    int X;
    int Y;
    boolean deleted=false;

    abstract public void Update();
    abstract public void Draw(Graphics2D g);

    public void setSpeedX(int speedX) {
        this.speedX = speedX;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public int getSpeedX() {
        return speedX;
    }

    public int getY() {
        return Y;
    }

    public int getX(){
        return X;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public boolean collides(GameObject other) {
        if(this.X+this.larghezza < other.X) return false;
        if(this.X > other.X+other.larghezza) return false;
        if(this.Y+this.altezza < other.Y) return false;
        if(this.Y > other.Y+other.altezza) return false;
        return true;
    }
}
