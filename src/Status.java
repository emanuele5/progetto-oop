import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Status implements Drawable {

    static BufferedImage life;
    static BufferedImage shield;

    static {
        try {
            life = ImageIO.read(new File("src/img/life.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static {
        try {
            shield = ImageIO.read(new File("src/img/shield.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    String status= "";
    int Points=0;
    int Lives=3;
    int Level=0;
    int Shields=5;

    public void Update() {
        status= "Livello: " +
                Level +
                " Punteggio: " +
                Points;
    }

    @Override
    public void Draw(Graphics2D g) {
        for(int i=0; i<Lives;++i){
            g.drawImage(life,400+(i*40)+45,400,null);
        }
        for(int i=0; i<Shields;++i){
            g.drawImage(shield,400+(i*40),350,null);
        }
        g.setColor(Color.WHITE);
        g.drawString(status,0,420);
    }

    public int getLives() {
        return Lives;
    }

    public void addLife() {
        Lives++;
    }

    public void removeLife() {
        Lives--;
    }

    public int getShields() {
        return Shields;
    }

    public void addShield() {
        Shields++;
    }

    public void removeShield() {
        Shields--;
    }

    public int getPoints() {
        return Points;
    }

    public void addPoints(int num) {
        Points+=num;
    }

    public int getLevel() {
        return Level;
    }

    public void addLevel() {
        Level++;
    }
}
