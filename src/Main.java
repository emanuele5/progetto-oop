import javax.sound.sampled.LineUnavailableException;
import javax.swing.*;
import java.awt.*;


public class Main
{
	public static void main(String[] args) {
		JFrame frame=new JFrame("progetto oop");
		frame.setSize(640,480);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setResizable(false);
		StartScreen startScreen = new StartScreen(frame);
		frame.setContentPane(startScreen);
		frame.setVisible(true);
	}

}
