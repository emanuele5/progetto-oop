import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class LevelUp extends GameObject{

    private int count=32;

    static BufferedImage levelup;

    static {
        try {
            levelup = ImageIO.read(new File("src/img/levelup.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public LevelUp() {
        larghezza=levelup.getWidth();
        altezza=levelup.getHeight();
        X=320-(larghezza/2);
        Y=480+altezza;
        speedY=-18;
    }

    @Override
    public void Update() {
        count--;
        if(count==0) {
            deleted=true;
        }
        Y+=speedY;
    }

    @Override
    public void Draw(Graphics2D g) {
        g.drawImage(levelup,X,Y,null);
        g.setColor(Color.cyan);
        g.setFont(new Font("DEBUG FREE TRIAL", Font.PLAIN, 40));
        g.drawString("LEVEL UP",220,240);
    }
}
