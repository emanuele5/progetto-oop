import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class StartScreen extends JPanel implements MouseListener {

    private final JFrame parent;

    public StartScreen(JFrame parent) {
        super();
        this.parent = parent;
        setBackground(Color.BLACK);
        addMouseListener(this);
        setFocusable(true);
        repaint();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        BackgroundImage.Draw1(g);
        g.setColor(Color.WHITE);
        g.setFont(new Font("DEBUG FREE TRIAL", Font.ITALIC, 25));
        g.drawRect(210,220,220,35);
        g.drawString("Inizia Il Gioco",220,250);
        g.drawRect(210,260,220,35);
        g.drawString("Classifica",220,290);
        g.drawRect(210,300,220,35);
        g.drawString("Crediti",220,330);
        g.setColor(Color.cyan);
        g.setFont(new Font("DEBUG FREE TRIAL", Font.PLAIN, 40));
        g.drawString("SPACE MONKEYS INVASION",45,200);
    }

    private void launchScreen(JPanel screen) {
        removeMouseListener(this);
        setFocusable(false);

        parent.setContentPane(screen);
        parent.revalidate();
        screen.requestFocusInWindow();
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        if(mouseEvent.getX()>=210 && mouseEvent.getX()<=430) {
            if(mouseEvent.getY()>=220 && mouseEvent.getY()<=255) {
                Game game=new Game(parent);
                launchScreen(game);
                game.start();
            }
            else if (mouseEvent.getY()>=260 && mouseEvent.getY()<=295) {
                launchScreen(new RankingScreen(parent));
            }
            else if (mouseEvent.getY()>=300 && mouseEvent.getY()<=335) {
                launchScreen(new CreditsScreen(parent));
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
