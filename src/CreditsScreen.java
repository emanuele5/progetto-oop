import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class CreditsScreen extends JPanel implements MouseListener {
    private final JFrame parent;

    public CreditsScreen(JFrame parent) {
        super();
        this.parent=parent;
        setBackground(Color.BLACK);
        addMouseListener(this);
        setFocusable(true);
        repaint();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        BackgroundImage.Draw1(g);
        g.setColor(Color.WHITE);
        g.setFont(new Font("DEBUG FREE TRIAL", Font.PLAIN, 30));
        g.drawString("Sviluppatori:",20,50);
        g.drawString("Clarissa Cavazzuti",20,85);
        g.drawString("Emanuele Davalli",20,120);
        g.drawRect(155,300,330,35);
        g.setFont(new Font("DEBUG FREE TRIAL", Font.ITALIC, 25));
        g.drawString("Torna al menù principale",160,330);
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        if(mouseEvent.getY()>=300 && mouseEvent.getY()<=335 && mouseEvent.getX()>=155 && mouseEvent.getX()<=486) {
            removeMouseListener(this);
            setFocusable(false);

            StartScreen screen=new StartScreen(parent);
            parent.setContentPane(screen);
            parent.revalidate();
            screen.requestFocusInWindow();
        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
