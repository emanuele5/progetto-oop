import javax.imageio.ImageIO;
import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

public class Alien extends GameObject implements Shooter{

    // sprite
    static BufferedImage sprite;
    private static final Random rndX=new Random();
    private static final Random rndY=new Random();
    private static Clip clip;

    static {
        try {
            sprite = ImageIO.read(new File("src/img/monkey.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static {
        try {
            AudioInputStream shootSound = AudioSystem.getAudioInputStream(new File("src/sounds/monkeycry.wav"));
            clip= AudioSystem.getClip();
            clip.open(shootSound);
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    public Alien(int lvl){
        larghezza = sprite.getWidth();
        altezza = sprite.getHeight();
        speedX = 1+lvl/3;
        speedY = 1+lvl/9;
        X=rndX.nextInt(641-this.larghezza);
        Y=rndY.nextInt((260*lvl+lvl+this.altezza))*-1;
    }

    @Override
    public void Update() {
        Y+=speedY;
        X+=speedX;
    }

    @Override
    public void Draw(Graphics2D g) {
        g.drawImage(sprite,null,X,Y);
    }

    public Laser Shoot() {
        if(Y>=0) {
            clip.setFramePosition(0);
            clip.loop(0);
        }
        return new AlienLaser(X+10, Y);
    }

    public Explosion Explode() {
        deleted=true;
        return new Explosion(X,Y);
    }
}

